﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

//-----------------------------Start of Script----------------------------------------------
public class buttonController : MonoBehaviour {

    //------------------------------Start(Not Used)--------------------------------------
	// Use this for initialization
	void Start () {
	
	}

    //-------------------------------Update(Not Used)------------------------------------
    // Update is called once per frame
    public void Update () {
	
	}

    //---------------------Button Controls----------------------------------------
    public void startGame()
    {
        SceneManager.LoadScene("Game Play");
    }

    public void quitGame()
    {
        Application.Quit();
    }

    public void loseScene()
    {
        SceneManager.LoadScene("Game Lose");
    }

    public void winScene()
    {
        SceneManager.LoadScene("Game Win");
    }

    public void exitToTitle()
    {
        SceneManager.LoadScene("Game Title");
    }
    //-----------------------------Start of Script----------------------------------------------
}
