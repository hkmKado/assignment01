﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

//-----------------------------Start of Script----------------------------------------------
public class moleScript : MonoBehaviour {

    //---------------------------Declaration-------------------------------
    public Animator moleAnim;
    private bool moleIsVisible = false;
    public AudioSource moleSound;
    private buttonController buttonCon;
    public bool MoleIsClicked = false;
   

    // Use this for initialization
    //--------------------------Start-----------------------------------------
    void Start() {
        moleAnim = GetComponent<Animator>();
        moleIsVisible = false;
        moleAnim.enabled = false;
        waitForRandomTime();
       
    }

    //---------------------------Update(Not Used)--------------------------------------
    // Update is called once per frame
    void Update() {

    }

    //----------------------------OnMouseDown--------------------------------
    void OnMouseDown()
    {

        if (moleIsVisible == true) {
            moleAnim.SetBool("Continue", true);
            moleAnim.SetBool("Wait", true);
            gameplayController.fScore += 1;
        moleSound.Play();
          
            moleIsVisible = false;
                  }
                      else{
            gameplayController.life -= 1;
    
              }
    }

    //---------------------Wait for time-------------------------------------------
        void waitForTime()
    {
        StartCoroutine(waitCoroutine());
    }
        void waitForTime2()
    {
        StartCoroutine(waitCoroutine2());
    }
        void waitForRandomTime()
    {
        StartCoroutine(waitRandomCoroutine());

    }
    
    //------------------------------IEnumerator---------------------------------------------------
    IEnumerator waitCoroutine()
    {
        yield return new WaitForSeconds(2);
        moleAnim.SetBool("Continue", true);
        moleAnim.SetBool("Wait", true);
    }

    IEnumerator waitCoroutine2()
    {
        int wait_RandomTime = Random.Range(1, 10);
        yield return new WaitForSeconds(wait_RandomTime);
        moleAnim.SetBool("Continue", false);
        moleAnim.SetBool("Wait", false);
        moleIsVisible = true;
    }

    IEnumerator waitRandomCoroutine()
    {
        int wait_RandomTime = Random.Range(1, 10);
        yield return new WaitForSeconds(wait_RandomTime);
        moleAnim.enabled = true;
        moleIsVisible = true;
    }

    //--------------------End of Script----------------------------
    }






