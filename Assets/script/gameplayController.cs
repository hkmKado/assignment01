﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

//-----------------------------Start of Script----------------------------------------------
public class gameplayController : MonoBehaviour {

    //---------------------------Declaration-------------------------------
    public Text txtScore;
    public Text txtLife;
    private moleScript moleSc;
    public static int fScore = 0;
    public static int life = 3;
    public Camera cam;
    GameObject lastHit;

    //----------------------------Start-----------------------
    // Use this for initialization
    void Start () {
       
    }
	
    //---------------------------Update--------------------------
	// Update is called once per frame
	void Update () {


        txtScore.text = "Score: " + fScore;
        txtLife.text = "Life: " + life;

        if(fScore >= 20)
        {
            fScore = 0;
            life = 3;
            SceneManager.LoadScene("Game Win");
            
        }
        if(life <= 0)
        {
            fScore = 0;
            life = 3;
            SceneManager.LoadScene("Game Lose");
           
        }

    }


    //-----------------------------End of Script----------------------------------------------
}



